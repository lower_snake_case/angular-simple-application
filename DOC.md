# Angular - Simple Application



## Foreword



## Requirements

### General Requirements

- Angular 8
- Navigation over three pages
  - Starting page with welcome message
  - User input for some data
  - Display data
- Data should persist a refresh
  - Store them locally
- Create README.md with instruction to run the application locally
- Upload code public and share the link

### Scope informations:

- Input can be anything
  - e.g. To-Do-App
  - "Don't hang yourself up on writing fancy input components"
- Keep it simple and stupid
  - no need for to impress with UX
  - just get it to work
- Avoid as much work as possible by using libraries (e.g. for the input)

### Bonus

-  Store data in a databank for choice
  - Use Prisma API to send and load data
    - Using the Apollo GraphQL client (can run locally)

## Preparation

### What is Angular in General?

> Angular is an app-design framework and development platform for creating efficient and sophisticated single-page apps.

[Introduction to the Angular Docs](https://angular.io/docs)

> Using Angular you can build amazing client-side applications using HTML, CSS, and Typescript.

[@ tektutorialshub > Complete Angular Tutorial For Beginners](https://www.tektutorialshub.com/angular-tutorial/)

> The Angular is the latest version of the AngularJS, which is a  development platform for building mobile and desktop web applications.  The Angular now comes with every feature you need to build a complex and sophisticated web or mobile application. It comes with features like [component](https://www.tektutorialshub.com/angular/angular-components/), [Directives](https://www.tektutorialshub.com/angular/angular-directives/), [Forms](https://www.tektutorialshub.com/angular/angular-forms-fundamentals/), [Pipes](https://www.tektutorialshub.com/angular/angular-pipes/), [HTTP Services](https://www.tektutorialshub.com/angular/angular-httpclient/), [Dependency Injection](https://www.tektutorialshub.com/angular/angular-dependency-injection/), etc

[@ tektutorialshub > Complete Angular Tutorial For Beginners](https://www.tektutorialshub.com/angular-tutorial/)

### Which benefits does Angular have?

#### Feature of Angular

1. Two-Way [Data Binding](https://www.tektutorialshub.com/angular/angular-data-binding/)

   > [...] Data binding is automatic and fast. changes made in the View is automatically updated in the component class and vice versa

   

2. Powerful [Routing](https://www.tektutorialshub.com/angular/angular-routing-navigation/) Support

   > The Angular Powerful routing engine loads the page asynchronously on the same page enabling us to create a Single Page Applications.

   

3. Expressive HTML

   > Angular enables us to use programming constructs like if conditions, for loops, etc to render and control how the HTML pages.

   

4. Modular by Design

   > Angular follows the modular design. You can create [Angular modules](https://www.tektutorialshub.com/angular/angular-modules/) to better organize and manage our codebase

   

5. Built-in Back End Support

   > Angular has built-in support to communicate with the back-end servers and execute any business logic or retrieve data

   

6. Active Community

### How does Angular work?

> It is very important to know how the Angular framework works before you start using it.
>
> 1. [Introduction to Angular](https://www.tektutorialshub.com/angular/angular-introduction/)
> 2. [Angular Architecture Overview & Concepts](https://www.tektutorialshub.com/angular/angular-architecture-overview-concepts/)

[@ tektutorialshub > Complete Angular Tutorial For Beginners](https://www.tektutorialshub.com/angular-tutorial/)

> Angular is completely written in Typescript and meets the ECMAScript 6  specification. This means that it has support for ES6 Modules, Class  frameworks, etc.

> In Angular, a component represents a UI element. You can have many such  components on a single web page. Each component is independent of each  other and manages a section of the page. The components can have child  components and parent components.

[@ tektutorialshub > Complete Angular Tutorial For Beginners](https://www.tektutorialshub.com/angular-tutorial/)

### How to use Angular 8?

- [Angular Docs](https://angular.io/docs)

### How to store data in Angular?

### How to handle user input?

### How to navigate over pages?

[Angular Docs > Getting Started with Angular: Routing](https://angular.io/start/start-routing)

### Which libraries are out there?

### Which libraries could have benefits for this project?

### What is the Prisma API?

### Why Prisma API?

### How to use the Prisma API?

### What is the Apollo GraphQL client?

### Why do i need only the client?

### Why Apollo GraphQL?

### How to use Apollo GraphQL?





## Getting started

To get hands on Angular I'd started to work throw the Angular documentations learning section

The learning section starts with some of the key features of Angular:

- `*ngFor`

  > ```html
  > <div *ngFor="let product of products">
  >     <!-- snip -->
  > </div>
  > ```

  the `*ngFor` directive acts like a for each loop

  

- `*ngIf`

  > ```html
  > <p *ngIf="product.description">
  >     Description: {{ product.description }}
  > </p>
  > ```

  the `*ngIf` directive is just a usual condition

  

- Interpolation `{{ }}`

  > ```html
  > <h3>
  >       {{ product.name }}
  > </h3>
  > ```

  Interpolation renders a property's value as text

  

- Property binding `[ ]`

  > ```html
  > <h3>
  >     <a [title]="product.name + ' details'">
  >       {{ product.name }}
  >     </a>
  > </h3>
  > ```

  property binding `[ ]` lets you use the property value in a template expression

  

- Event binding `( )`

  > ```html
  > <button (click)="share()">
  >     Share
  > </button>
  > ```

  If an event is called (`(click)`) a binded function (`share()`) will be called
  
  [@ angular docs > learn](https://angular.io/start)

### Components

> *Components* define areas of responsibility in the user interface, or UI, that let you reuse sets of UI functionality.

> A component consists of three things:
>
> 1. **A component class** that handles data and functionality. [...]
> 2. **An HTML template** that determines the UI. [...]
> 3. **Component-specific styles** that define the look and feel. [...]
>
> An Angular application comprises a tree of components, in which each  Angular component has a specific purpose and responsibility.

### Input

#### Import input dependency

> *src/app/product-alerts/product-alerts.component.ts*
>
> ```typescript
> import { Input } from '@angular/core';
> ```



#### Add input property for input

> *src/app/product-alerts/product-alerts.component.ts*
>
> ```typescript
> export class ProductAlertsComponent implements OnInit {
>   @Input() product; // new input property
>   constructor() { }
> 
>   ngOnInit() {
>   }
> 
> }
> ```



#### Define the view for the new product alert component

> *src/app/product-alerts/product-alerts.component.html*
>
> ```typescript
> <p *ngIf="product.price > 700">
>   <button>Notify Me</button>
> </p>
> ```



#### Display the new product alert component as a child of the product list

> *src/app/product-list/product-list.component.html*
>
> ```typescript
> /*
> 	-- snip --
> */
> 
> <app-product-alerts [product]="product">
>   </app-product-alerts>
> ```



### Output

#### Import Output and EventEmitter dependencies

> *src/app/product-alerts/product-alerts.component.ts*
>
> ```typescript
> import { Output, EventEmitter } from '@angular/core';
> ```



#### Define a new property with a @Output() decorator which is an new instance of EventEmitter

> src/app/product-alerts/product-alerts.component.ts
>
> ```typescript
>    export class ProductAlertsComponent {
> @Input() product;
>   @Output() notify = new EventEmitter(); // new property
>   }
> ```

### Binding the event in the UI (.html) of the component

> src/app/product-alerts/product-alerts.component.html
>
> ```html
> <p *ngIf="product.price > 700"> <!-- If products is more expensiv then 700$ -->
>   <button (click)="notify.emit()">Notify Me</button> <!-- Show a button with an action -->
> </p>
> ```



#### Define the behavior of the button click

> *src/app/product-list/product-list.component.ts*
>
> ```typescript
>    export class ProductListComponent {
> 
>   /*
>   	-- snip --
>   */
>   
> onNotify() {
>    window.alert('You will be notified when the product goes on sale');
>    }
>   }
> ```



#### Binding the button event to the onNotify() function

> src/app/product-list/product-list.component.html
>
> ```html
> <!-- snip -->
> 
> <app-product-alerts
>   [product]="product" 
>   (notify)="onNotify()"> <!-- Binding -->
> </app-product-alerts>
> ```



### Routing



> [...] The router enables navigation from one view to the next as users perform tasks such as the following:
>
> - Entering a URL in the address bar to navigate to a corresponding page.
> - Clicking links on the page to navigate to a new page.
> - Clicking the browser's back and forward buttons to navigate backward and forward through the browser history.



Implementing product details for each product in separate pages with different URLs



1.  #### Adding a new component for product details

   - right-click the `app` folder
   - Angular Generator
   - Component
   - name `product-details`

2. #### Associate URLs (`product/*`) with component

   - *src/app/app.module.ts*

     ```typescript
     @NgModule({
       imports: [
         BrowserModule,
         ReactiveFormsModule,
         RouterModule.forRoot([
           { path: '', component: ProductListComponent },
           { path: 'products/:productId', component: ProductDetailsComponent },
         ])
       ],
     ```

   

3.  #### Binding `prductId` value

   - Update the `*ngFor` directive to assign each index in the `products` array to the `productId` variable when iterating over the list

   - Modify the product name anchor to include a `routerLink`

     

     *src/app/product-list/product-list.component.html*

     ```html
     <div *ngFor="let product of products; index as productId">
     
       <h3>
         <a [title]="product.name + ' details'" [routerLink]="['/products', productId]">
           {{ product.name }}
         </a>
       </h3>
     <!-- . . . -->
     </div>
     ```

     > The RouterLink directive gives the router control over the anchor  element. In this case, the route, or URL, contains one fixed segment, `/products`, while the final segment is variable, inserting the id property of the current product

   

4. #### Using route Information

   > [...] This section shows you how to use the Angular Router to combine the `products` data and route information to display the specific details for each product

   - ##### Arranging to use external product data
     - Import `ActivatedRoute` from the `@angular/router` package, and the `products` array from `../products

       

       *src/app/product-details/product-details.component.ts*

       ```typescript
       import { Component, OnInit } from '@angular/core';
       import { ActivatedRoute } from '@angular/router';
       
       import { products } from '../products';
       ```

     

     - Define the `product` property and inject the `ActivatedRoute` into the constructor by adding it as an argument within the constructor's parentheses

       

       *src/app/product-details/product-details.component.ts*

       ```typescript
       export class ProductDetailsComponent implements OnInit {
         product;
       
         constructor(
           private route: ActivatedRoute,
         ) { }
       
       }
       ```

       > The `ActivatedRoute` is specific to each routed component that the Angular Router loads. It contains information about the route, its parameters, and additional data associated with the route.
       >
       > By injecting the `ActivatedRoute`, you are configuring the component to use a service

       

5.  #### Fetching product data 

   - In the `ngOnInit()` method, subscribe to route parameters and fetch the product based on the `productId`

     

     *src/app/product-details/product-details.component.ts*

     ```typescript
     ngOnInit() {
       this.route.paramMap.subscribe(params => {
         this.product = products[+params.get('productId')];
       });
     }
     ```

     > [...] Angular uses the `productId` to display the details for each unique product

6. #### Displaying product details

   - Update the template to display product details information inside an `*ngIf`

     

     *src/app/product-details/product-details.component.html*

     ```html
     <h2>Product Details</h2>
     
     <div *ngIf="product">
       <h3>{{ product.name }}</h3>
       <h4>{{ product.price | currency }}</h4>
       <p>{{ product.description }}</p>
     
     </div>
     ```

     

> For more information about the Angular Router, see [Routing & Navigation](https://angular.io/guide/router).



[@ angular docs > Getting Started with Angular: Routing](https://angular.io/start/start-routing)



## Installing Angular CLI 8

### About my server

```bash
$ uname -orm
4.15.0 x86_64 GNU/Linux
```

```bash
$ lsb_release -a
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.4 LTS
Release:	18.04
Codename:	bionic
```



### Installing Node.js

```bash
$ sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
$ sudo apt-get install -y nodejs
```

#### Node.js and NPM versions

```bash
$ nodejs -v
v13.12.0
$ npm -v
6.14.4
```



### Installing Angular CLI

```bash
$ npm install -g @angular/cli@8
$ npm install @angular-devkit/build-angular
```

#### Angular CLI version

```bash
$ ng version
     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 8.3.26
Node: 13.13.0
OS: linux x64
Angular: 
... 

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.803.26
@angular-devkit/core         8.3.26
@angular-devkit/schematics   8.3.26
@schematics/angular          8.3.26
@schematics/update           0.803.26
```



## Deployment

### Building production version

```bash
$ ng build --prod
```



### Installing lite-server

```bash
$ sudo npm install -g lite-server
```



### Run project on lite-server

```bash
$ cd <PROJECT_DIR>/dist/<APPLICATION>
$ lite-server
```



## Installing Prisma/GraphQL Server

### Installing Prisma

```bash
$ sudo npm install -g prisma
```



### Creating and setting up a project

```bash
$ mkdir <DIR>
$ cd <DIR>
$ prisma init .
```

Choose `GraphQL server/full stack boilerplate (recommend)`.

Next choose `typescript-advanced` because of the authentication advantage. (if needed)

#### Cluster

1. [Install Docker on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
2. Choose `local`



#### Information about the server

```bash
$ prisma info
```



Now you can just begin to start to edit your server with any kind of IDE or text editor.



## Storing data locally

### Requirement

> Now If you build the application you will get the following error
>
> ERROR in src/app/reading-json-files/reading-json-files.component.ts(2,24):  error TS2732: Cannot find module ‘../../assets/SampleJson.json’.  Consider using ‘–resolveJsonModule’ to import module with ‘.json’  extension
>
> To remove the above error,In tsconfig.json  file under compiler options we need to add “resolveJsonModule”  and ”esModuleInterop” configurations as true as shown below.
>
> ```json
> {  "compilerOptions": {  "resolveJsonModule": true, "esModuleInterop": true } }
> ```
>
> One more thing you need to understand is the the imported JSON file contents are typesafe. In the above json version is a number. So if you tried to change the version to string it will throw error.
>
> ```typescript
> SampleJson.Version= "Seven"; //throws error as Version is of 
> ```
>
>  [@Angular Wiki > How To Read Local JSON Files In Angular](https://www.angularjswiki.com/angular/how-to-read-local-json-files-in-angular/)



### Storing data in local storage

The data will be stored pair with strings as key and value. So we'll need to cast our array to a JSON string.

```typescript
localStorage.setItem(
      'todos',
      JSON.stringify(this.todos)
);
```



### Retrieving data from local storage

We'll need to parse the data from JSON in order to use them as array.

```typescript
this.todos = JSON.parse (localStorage.getItem('todos'));
```



## Install & run HTTP-Server

1. Connect to your server

2. Pull your project from Git or via SCP

3. Go to your project path

   ```bash
   $ cd <PROJECT_DIR>
   ```

4. Install dependencies for your project

   ```bash
   $ sudo npm install
   ```

4. Install a http server

   ```bash
   $ npm install http-server
   ```

5. Create unprivileged user to run the server later on

   ```bash
   $ useradd -s /bin/sh <USER>
   ```

6. Disconnect from your server

7. Start the http server

   ```bash
   ssh <USER>@<IP/URL> -p<PORT> http-server <ABSOLUTE_PROJOCET_PATH>
   ```

   

## References

- [Complete Angular Tutorial For Beginners @ tektutorialshub](https://www.tektutorialshub.com/angular-tutorial/)
- [Introduction to Angular @ tektutorialshub](https://www.tektutorialshub.com/angular/angular-introduction/)
- [Angular Docs](https://angular.io/docs)
- [Getting Started with Angular: Your First App @ Angular Docs](https://angular.io/start/)
- [Getting Started with Angular: Routing @ Angular Docs](https://angular.io/start/start-routing)
- [StackBlitz](https://stackblitz.com/angular/odpeknvxnlq?file=src%2Fapp%2Fapp.component.ts)
- [Basic Syntax @ Markdown](https://www.markdownguide.org/basic-syntax)
- [Node.js @ GitHub](https://github.com/nodesource/distributions#debinstall)
- [How to install Node.js on Ubuntu @ Linuxize](https://linuxize.com/post/how-to-install-node-js-on-ubuntu-18.04/)
- [How to Work with Angular and MySQL @ okta](https://developer.okta.com/blog/2019/08/16/angular-mysql-express)
- [Angular CLI @ NPM.js](https://www.npmjs.com/package/@angular/cli)
- [How to build an Angular 8 app from scratch in 11 easy steps @ freeCodeCamp](https://www.freecodecamp.org/news/angular-8-tutorial-in-easy-steps/)
- [techiediaries @ GitHub](https://github.com/techiediaries?after=Y3Vyc29yOnYyOpK0MjAxOS0wMy0wMlQyMDozNTowMlrOCldVOA%3D%3D&tab=repositories)
- [Learn Angular 8 from Scratch for Beginners - Crash Course @ YouTube](https://www.youtube.com/watch?v=_TLhUCjY9iA)
- [Setting up a Prisma GraphQL Server - Part 0 @ YouTube](https://www.youtube.com/watch?v=uyOI-hUU00A)
- [Angular 7 Local Storage @ YouTube](https://www.youtube.com/watch?v=QsWqTswEjgU)
- [GraphQL Fundamentals @ HowToGraphQL](https://www.howtographql.com)
- [DuckDNS](https://duckdns.org)
- [Linux Run Command As Another User @ cyberciti](https://www.cyberciti.biz/open-source/command-line-hacks/linux-run-command-as-different-user/)
- [How to Execute Linux Commands on Remote System over SSH](https://www.linuxtechi.com/execute-linux-commands-remote-system-over-ssh/)
- [http-server @ NPM](https://www.npmjs.com/package/http-server)

