# Angular - Simple Application



## Table of contents

1. [Instructions to run the application](#instructions-to-run-the-application)
2. [More about my project](#more-about-my-project)





## Instructions to run the application

1. Open the application which is already running on my server
   1. Open your browser
   2. Navigate to http://foxbase.duckdns.org:8080
2. Or run it locally
   1. Pull the repository
   2. Go to the project folder
   3. Install dependencies by running `npm install`
   4. Run the developer server by typing `ng serve` into your terminal (or create an production build via `ng build --prod` and run the http server of your choice inside the `./dist/todo-app` directory)
   5. Open http://localhost:4200 in your bowser





## More about my project

If you want to know more about this project feel free to visit my [personnel documentation](./DOC.md). ;)