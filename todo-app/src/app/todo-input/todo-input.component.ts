import { Component, OnInit } from '@angular/core';

import { Todo } from '../interfaces/todo';
import { TodoComponent } from '../todo/todo.component';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.scss']
})
export class TodoInputComponent implements OnInit {

  todos:   Todo [];
  nextId:  number;
  content: string;

  constructor() { }

  ngOnInit() {

    this.getTodos();

    (this.todos.length > 0) ? this.nextId = this.todos[this.todos.length - 1].id + 1 : this.nextId = 1;
  }

  getTodos(): void {

    if (localStorage.getItem('todos') === null) {
      this.todos = [];
    }
    else {
      this.todos = JSON.parse (localStorage.getItem('todos'));
    }
  }

  setTodos(): void {

    localStorage.setItem(
      'todos',
      JSON.stringify(this.todos)

    );

  }

  pushback (): void {

      if (this.content.length === 0) {
        return;
      }

      this.todos.push (
        {
          'id':        this.nextId,
          'content':   this.content,
          'completed': false,
          'editing':   false
        }
      );

      this.content = '';
      this.nextId++;

      this.setTodos ();
  }

}
