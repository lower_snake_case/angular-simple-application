import { Component, OnInit } from '@angular/core';

import { Todo } from '../interfaces/todo';

@Component({
  selector: 'app-todo-output',
  templateUrl: './todo-output.component.html',
  styleUrls: ['./todo-output.component.scss']
})
export class TodoOutputComponent implements OnInit {

  todos:         Todo [];
  content:       string;
  contentBackup: string;

  constructor() { }

  ngOnInit() {
    // fetching todos from local storage
    this.getTodos();
  }

  getTodos(): void {

    if (localStorage.getItem('todos') === null) {
      this.todos = [];
    }
    else {
      this.todos = JSON.parse (localStorage.getItem('todos'));
    }
  }

  setTodos(): void {

    localStorage.setItem(
      'todos',
      JSON.stringify(this.todos)

    );

  }


  edit(todo: Todo): void {

    this.contentBackup = todo.content;
    todo.editing       = true;

  }

  remove(id: number): void {
    this.todos = this.todos.filter(todo => todo.id != id);
    this.setTodos();

  }

  changeCompleted (todo: Todo): void {
    todo.completed = !todo.completed;
    this.setTodos();

  }

  rollback (todo: Todo): void {
    todo.content = this.contentBackup;
    todo.editing = false;

  }

}
